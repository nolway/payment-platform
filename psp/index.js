const express = require('express');
const https = require('https');
const fs = require('fs');
const cors = require('cors');

const port = 3000;

const options = {
  key: fs.readFileSync('selfsigned.key'),
  cert: fs.readFileSync('selfsigned.crt')
};

const app = express();

app.use(cors());
app.use(express.json());

app.post('/hello', (req, res) => {
  res.send('fdsgdfg');
});

app.post('/payment', (req, res) => {
  setTimeout(() => {
    res.status(200).json({
      status: 'accepted'
    });
  }, 10000);
});

app.post('/refund', (req, res) => {
  setTimeout(() => {
    res.status(200).json({
      status: 'accepted'
    });
  }, 10000);
});

/* https.createServer(options, app).listen(3000); */
app.listen(port, () => {
  console.log(`PSP Started at http://localhost:${port}`);
});