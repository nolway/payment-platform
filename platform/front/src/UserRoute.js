import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import useAuth from './hooks/useAuth';

const UserRoute = ({ component: Component, ...rest }) => {
    const {selectors} = useAuth();
    let isUser;

    if (selectors.userInfo()) {
        isUser = selectors.userInfo().role === 0;
    }

    return (
        <Route
            {...rest}
            render={props =>
                selectors.isLogged() && isUser ? (
                    <Component {...props} />
                ) : (
                    <Redirect to="/signin" />
                )
            }
        />
    );
}

export default UserRoute;