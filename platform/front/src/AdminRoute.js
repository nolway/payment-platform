import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import useAuth from './hooks/useAuth';

const AdminRoute = ({ component: Component, ...rest }) => {
    const {selectors} = useAuth();
    let isAdmin;

    if (selectors.userInfo()) {
        isAdmin = selectors.userInfo().role === 1;
    }

    return (
        <Route
            {...rest}
            render={props =>
                selectors.isLogged() && isAdmin ? (
                    <Component {...props} />
                ) : (
                    <Redirect to="/signin" />
                )
            }
        />
    );
}

export default AdminRoute;