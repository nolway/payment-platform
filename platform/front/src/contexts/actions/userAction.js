import {BASE_API} from '../../Constants';

export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_USER = 'FETCH_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const DELETE_USER = 'DELETE_USER';

export const fetchUsers = (token) => 
    fetch(`${BASE_API}/users`, {
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    }).then(res => res.json());


export const fetchUser = (token, userId) => 
    fetch(`${BASE_API}/users/${userId}`, {
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    }).then(res => res.json());
;

export const updateUser = (token, userId, param) => 
    fetch(`${BASE_API}/users/${userId}`, {
        method: 'PUT',
        body: JSON.stringify(param),
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())


export const deleteUser = (token, userId) => 
    fetch(`${BASE_API}/users/${userId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    })
    .then(res => console.log(res.text()));

