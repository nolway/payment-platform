import {PSP_API} from '../../Constants';

export const CHECKING = 'CHECKING';

export const checking = (param) => 
    fetch(`${PSP_API}/checking`, {
        method: 'POST',
        body: JSON.stringify(param),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json());
