import React from 'react';
import {Switch, Route, withRouter, Redirect} from 'react-router-dom';
import Dashboard from './pages/Dashboard/Dashboard';
import Home from './pages/Home/Home';
import Signup from './pages/Signup/Signup';
import Signin from './pages/Signin/Signin';
import Signout from './pages/Signout/Signout';
import Payment from './pages/Payment/Payment';
import SignupConfirm from './pages/SignupConfirm/SignupConfirm';
import MerchantTable from './pages/Admin/Merchant/MerchantTable';
import AdminTransactionTable from './pages/Admin/Transaction/TransactionTable';
import UserTable from './pages/Admin/User/UserTable';
import MerchantDetail from './pages/Admin/Merchant/MerchantDetail';
import UserDetail from './pages/Admin/User/UserDetail';
import Profile from './pages/Profile/Profile';
import TransactionTable from './pages/Transaction/TransactionTable';
import UserRoute from './UserRoute';
import AdminRoute from './AdminRoute';
import Header from './components/Header/Header';
import Drawer from './components/Drawer/Drawer';
import AdminDashboard from './pages/Admin/Dashboard/Dashboard';
import useAuth from './hooks/useAuth';
import NotFound from './pages/NotFound/NotFound';

const Router = (props) => {
    const {selectors} = useAuth();
    let userRole;
    if (selectors.userInfo()) {
        userRole = selectors.userInfo().role;
    }

    const hideDrawer = ['/signin', '/signup', '/signup-confirmation', '/signout', '/', '/payment'];
    const hideHeader = ['/payment'];

    return (
        <React.Fragment>
            {
                hideHeader.includes(props.location.pathname) ? '' : <Header />
            }
            {
                hideDrawer.includes(props.location.pathname) ? '' : 
                selectors.isLogged() ? <Drawer /> : ''                
            }
            <Switch>
                {/* <Route path="/" component={Home} /> */}
                <Route exact path="/" component={Home} />
                <Route exact path="/signup" component={Signup} />
                <Route exact path="/signin" component={Signin} />
                <Route exact path="/signup-confirmation" component={SignupConfirm} />
                <Route exact path="/signout" component={Signout} />
                <Route exact path="/payment" component={Payment} />
                
                <UserRoute exact path="/dashboard" component={Dashboard} />
                <UserRoute exact path="/profile" component={Profile} />
                <UserRoute exact path="/transactions" component={TransactionTable} />
                
                <AdminRoute exact path="/admin/dashboard" component={AdminDashboard} />
                <AdminRoute exact path="/admin/merchants" component={MerchantTable} />
                <AdminRoute exact path="/admin/transactions" component={AdminTransactionTable} />
                <AdminRoute exact path="/admin/users" component={UserTable} />
                <AdminRoute exact path="/admin/users/:id" component={UserDetail} />
                <AdminRoute exact path="/admin/merchants/:id" component={MerchantDetail} />
                <Route component={NotFound} />
            </Switch>
        </React.Fragment>
    );
}

export default withRouter(Router);
