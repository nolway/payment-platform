import {validateNotEmpty, validateEmail} from './index';

export const validate = values => {
    let errors = {};

    errors.email = validateEmail(values.email);
    errors.password = validateNotEmpty(values.password);

    return errors;
}