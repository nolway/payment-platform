import React from 'react';
import {Link} from 'react-router-dom';
import useAuth from '../../hooks/useAuth';

const Drawer = () => {
    const {selectors} = useAuth();
    let isUser;
    if (selectors.userInfo()) {
        isUser = selectors.userInfo().role === 0;
    }

    function renderDrawerLinks() {
        if (isUser) {
            return <React.Fragment>
                <Link to="/dashboard">
                    <span className="flex items-center p-4 hover:bg-blue-500 hover:text-white">
                        Dashboard
                    </span>
                </Link>
                <Link to="/transactions">
                    <span className="flex items-center p-4 hover:bg-blue-500 hover:text-white ">
                        Mes transactions
                    </span>
                </Link>
            </React.Fragment>
        } else {
            return <React.Fragment>
                <Link className="flex items-center p-4 hover:bg-blue-500 hover:text-white" to="/admin/dashboard">
                    Dashboard
                </Link>
                <Link className="flex items-center p-4 hover:bg-blue-500 hover:text-white" to="/admin/merchants">
                    Liste des marchands
                </Link>
                <Link className="flex items-center p-4 hover:bg-blue-500 hover:text-white" to="/admin/users">
                    Liste des utilisateurs
                </Link>
                <Link className="flex items-center p-4 hover:bg-blue-500 hover:text-white" to="/admin/transactions">
                    Liste des transactions
                </Link>
            </React.Fragment>
        }
    }

    return (
        <React.Fragment>
            <aside className="transform border-r top-0 left-0 w-64 bg-white fixed h-full overflow-auto z-30">
                <span className="flex w-full items-center p-8 border-b"></span>
                {renderDrawerLinks()}            
            </aside>
        </React.Fragment>
    )
}

export default Drawer;