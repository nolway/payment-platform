import React from 'react';
import {useMemo} from 'react';

const Alert = ({className, role, text, color}) => {
    const addClasses = className !== undefined ? ` ${className}` : '';
    return useMemo(() =>
        <>
            <div
                className={`bg-${color}-100 border border-${color}-400 text-${color}-700 px-4 py-3 rounded relative mt-4 ${addClasses}`}
                role={role}
            >
                <span className="block sm:inline">{text}</span>
            </div>
        </>,
        [
            className,
            role,
            text,
            color
        ]
    );
}

export default Alert;