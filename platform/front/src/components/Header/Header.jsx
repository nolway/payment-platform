import React from 'react';
import {Link} from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
import Button from '../Button/Button';
const Header = () => {
    const {selectors} = useAuth();
    let isUser;

    if (selectors.userInfo()) {
        isUser = selectors.userInfo().role === 0;
    }

    function renderLinks() {
        if (selectors.isLogged()) {
            return <React.Fragment>
                <div className="dropdown inline-block relative">
                    <Button className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue-500 hover:bg-white mt-4 mr-4 lg:mt-0">
                        <svg className="" xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                            <circle cx="12" cy="7" r="4"></circle>
                        </svg>
                    </Button>
                    <ul className="dropdown-menu absolute hidden text-gray-700 pt-1 right-0">
                        {isUser &&
                            <li><Link className="rounded-t bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" to="/profile">Mes informations</Link></li>
                        }
                        <li><Link className="rounded-b bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" to="/signout">Déconnexion</Link></li>
                    </ul>
                </div>
            </React.Fragment>
        } else {
            return <React.Fragment>
                <ul className="flex">
                    <li><Link to="/signin" className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue-500 hover:bg-white mt-4 mr-4 lg:mt-0">Connexion</Link></li>
                    <li><Link to="/signup" className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue-500 hover:bg-white mt-4 lg:mt-0">Inscription</Link></li>
                </ul>
            </React.Fragment>
        }   
    }

    return (
        <nav className={`${selectors.isLogged() && 'ml-64'} flex items-center justify-between flex-wrap bg-blue-500 p-4`}>
            <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
                <div className="text-sm lg:flex-grow">
                   {/*  <a href="#" className="block mt-4 lg:inline-block lg:mt-0 text-blue-200 hover:text-white mr-4">
                        Link 1
                    </a> */}
                </div>
                {renderLinks()}
            </div>
        </nav>
    );
}

export default Header;