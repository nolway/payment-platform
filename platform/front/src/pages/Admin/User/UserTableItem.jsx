import React from 'react';
import {Link} from 'react-router-dom';
import Moment from 'moment';

const UserTableItem = (props) => {
    const {user} = props;
    const formattedDate = Moment(user.created_at).format('DD/MM/YYYY');

    return (
        <tr className="border-b hover:bg-orange-100 cursor-pointer transition duration-200 ease-in-out">
            <td><Link className="block p-3 px-5" to={`/admin/users/${user.id}`}>{user.email}</Link></td>
            <td><Link className="block p-3 px-5" to={`/admin/users/${user.id}`}>{user.createdAt}</Link></td>
            <td>
                <Link className="block p-3 px-5" to={`/admin/users/${user.id}`}>
                    <span className={`${user.active ? 'bg-green-400' : 'bg-red-400'} rounded py-1 px-3 text-xs text-white font-bold`}>
                        {user.active ? 'Activé' : 'Désactivé'}
                    </span>
                </Link>
            </td>
        </tr>
    );
};

export default UserTableItem;