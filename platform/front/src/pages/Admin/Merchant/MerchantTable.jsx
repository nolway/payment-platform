import React, {useEffect, useState, useLayoutEffect} from 'react';
import TextField from '../../../components/TextField/TextField';
import MerchantTableItem from './MerchantTableItem';
import useMerchant from '../../../hooks/useMerchant';

const MerchantTable = () => {
    const {merchantSelectors, merchantActions} = useMerchant();
    const token = localStorage.getItem('token');
    const [filterMerchants, setFilterMerchants] = useState([]);
    const merchants = merchantSelectors.getMerchants();

    const getMerchantTable = async () => {
        const merchants = await merchantSelectors.getMerchants();
        setFilterMerchants(merchants);
    }

    useEffect(() => {
        merchantActions.fetchMerchants(token);
        getMerchantTable();
    }, []);

    function renderMerchantList() {
        if (filterMerchants.length > 0) {
            return filterMerchants.map(merchant => <MerchantTableItem key={merchant.id} merchant={merchant} />);
        }
        return (
            <tr><td className="text-center" colSpan={5}>Il n'y a pas de marchand actuellement.</td></tr>
        );
    }

    function onSearch(event) {
        setFilterMerchants(merchantSelectors.filterMerchants(event.target.value));
    }

    return (
        <div className="ml-64 bg-gray-300">
            <div className="max-w-full w-full lg:max-w-full p-8 h-screen">
                <div className="overflow-x-auto bg-white rounded-lg p-4 flex flex-col justify-between leading-normal shadow-lg">
                    <p className="text-gray-900 font-bold text-xl mb-6 border-b">Liste des marchands</p>
                    <div className="mb-4">
                        <label className="inline uppercase tracking-wide text-gray-700 text-xs font-bold mr-2">Rechercher : </label>
                        <TextField
                            className="mt-2 w-64 appearance-none inline bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-3 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            type="search"
                            name="search"
                            onChange={onSearch}
                            placeholder="Raison social"
                        />
                    </div>
                    <table className="w-full text-md bg-white shadow rounded mb-4">
                        <thead>
                            <tr className="bg-gray-200 border-b">
                                <th className="text-left p-3 px-5">Raison sociale</th>
                                <th className="text-left p-3 px-5">SIRET</th>
                                <th className="text-left p-3 px-5">phone</th>
                                <th className="text-left p-3 px-5">Date création</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderMerchantList()}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default MerchantTable;