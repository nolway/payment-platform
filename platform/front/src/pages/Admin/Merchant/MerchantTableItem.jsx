import React from 'react';
import {Link} from 'react-router-dom';
import Moment from 'moment';

const MerchantTableItem = (props) => {
    const {merchant} = props;
    const formattedDate = Moment(merchant.created_at).format('DD/MM/YYYY');

    return (
        <tr className="border-b hover:bg-orange-100 cursor-pointer transition duration-200 ease-in-out">
            <td><Link className="block p-3 px-5" to={`/admin/merchants/${merchant.id}`}>{merchant.social_reason}</Link></td>
            <td><Link className="block p-3 px-5" to={`/admin/merchants/${merchant.id}`}>{merchant.siret}</Link></td>
            <td><Link className="block p-3 px-5" to={`/admin/merchants/${merchant.id}`}>{merchant.phone}</Link></td>
            <td><Link className="block p-3 px-5" to={`/admin/merchants/${merchant.id}`}>{formattedDate}</Link></td>
        </tr>
    );
};

export default MerchantTableItem;