import React, {useEffect, useState} from 'react';
import useTransaction from '../../../hooks/useTransaction';
import TextField from '../../../components/TextField/TextField';
import TransactionTableItem from './TransactionTableItem';

const TransactionTable = () => {
    const {transactionSelectors, transactionActions} = useTransaction();
    const token = localStorage.getItem('token');
    const [filterTransactions, setFilterTransactions] = useState([]);
    const transactions = transactionSelectors.getTransactions();

    useEffect(() => {
        transactionActions.fetchTransactions(token);
        if (transactions) {
            setFilterTransactions(transactions);
        }
    }, []);

    const renderTransactionTable = () => {
        if (filterTransactions.length > 0) {
            return filterTransactions.map(transaction => <TransactionTableItem key={transaction.id} transaction={transaction} />);
        }
        return (
            <tr><td className="text-center" colSpan={5}>Il n'y a pas de transaction actuellement.</td></tr>
        );
    }

    function onSearch(event) {
        setFilterTransactions(transactionSelectors.filterTransactions(event.target.value));
    }

    return (
        <div className="ml-64 bg-gray-300">
            <div className="max-w-full w-full lg:max-w-full p-8 h-screen">
                <div className="overflow-x-auto bg-white rounded-lg p-4 flex flex-col justify-between leading-normal shadow-lg">
                    <p className="text-gray-900 font-bold text-xl mb-6 border-b">Mes transactions</p>
                    <div className="mb-4">
                        <label className="inline uppercase tracking-wide text-gray-700 text-xs font-bold mr-2">Rechercher : </label>
                        <TextField
                            className="mt-2 w-64 appearance-none inline bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-3 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            type="search"
                            name="search"
                            onChange={onSearch}
                            placeholder="Montant"
                        />
                    </div>
                    
                    <table className="w-full text-md bg-white shadow rounded mb-4">
                        <thead>
                            <tr className="bg-gray-200 border-b">
                                <th className="text-left p-3 px-5">Montant</th>
                                <th className="text-left p-3 px-5">Devise</th>
                                <th className="text-left p-3 px-5">Statut</th>
                                <th className="text-left p-3 px-5">Token</th>
                                <th className="text-left p-3 px-5">Date de transaction</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderTransactionTable()}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default TransactionTable;