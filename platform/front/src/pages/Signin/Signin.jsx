import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';
import TextField from '../../components/TextField/TextField';
import Button from '../../components/Button/Button';
import Alert from '../../components/Alert/Alert';
import useAuth from '../../hooks/useAuth';
import useForm from '../../hooks/useForm';
import useLoading from '../../hooks/useLoading';
import {validate} from '../../validations/LoginFormValidation';
import ClipLoader from 'react-spinners/ClipLoader';
import {Redirect} from 'react-router-dom';

const Signin = () => {
    const {actions, selectors} = useAuth();
    const {isLoading, startLoading, setIsLoading} = useLoading();
    const {
        handleChange,
        handleSubmit,
        errors,
        inputFormValues
    } = useForm(onLogin, validate);

    useEffect(() => {
        selectors.errorMsg() && setIsLoading(false);
    });

    function onLogin() {
        startLoading()
        actions.login({...inputFormValues});
    };
    
    if (selectors.isLogged()) {
        if (selectors.userInfo().role === 0) {
            return <Redirect to="/dashboard" />;
        } else {
            return <Redirect to="/admin/dashboard" />;
        }
    }

    return (
        <React.Fragment>
            <div className="flex justify-center mt-12">
                <form onSubmit={handleSubmit} className="w-full max-w-lg rounded shadow-lg p-8">
                    <p className="text-2xl mb-3">Connectez-vous à votre compte</p>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3 mb-6">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">
                                E-mail
                            </label>
                            <TextField
                                className={`${errors.password ? 'border-red-500' : ''} appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500`}
                                id="email"
                                type="text"
                                name="email"
                                placeholder="goxow78665@unknmail.com"
                                onChange={handleChange}
                                value={inputFormValues.email || ''}
                                error={errors.email}
                            />
                        </div>
                        <div className="w-full px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="password">
                                Mot de passe
                            </label>
                            <TextField
                                className={`${errors.password ? 'border-red-500' : ''} appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500`}
                                id="password"
                                type="password"
                                name="password"
                                placeholder="*****************"
                                onChange={handleChange}
                                value={inputFormValues.password || ''}
                                error={errors.password}
                            />
                            {selectors.errorMsg() &&
                                <Alert
                                    color="red"
                                    role="alert"
                                    text={selectors.errorMsg()}
                                />
                            }
                        </div>
                    </div>
                    <div className="text-center">
                        <Button
                            type="submit"
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full w-full px-3 mb-6 md:mb-0"
                        >
                            {/* {isLoading ? <ClipLoader
                                size={24}
                                color={"#fff"}
                                loading={isLoading}
                            /> : 'Connexion'} */}
                            Connexion
                        </Button>
                    </div>
                    <div className="text-center mt-2">
                        <p className="text-base">Vous n'avez pas de compte ? {' '}
                            <Link className="underline text-blue-600" to="/signup">Je m'inscris</Link>
                        </p>
                    </div>
                </form>
            </div>
        </React.Fragment>
    );
}

export default Signin;