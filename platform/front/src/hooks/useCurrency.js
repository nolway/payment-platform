import {useContext} from 'react';
import RootContext from '../contexts/rootContext';
import {
    fetchCurrencies as aFetchCurrencies,
    FETCH_CURRENCIES,
} from '../contexts/actions/currencyAction';

const useCurrency = () => {
    const {
        state: {
            currency: currencyState
        },
        dispatch,   
    } = useContext(RootContext);
    
    const currencyActions = {
        fetchCurrencies: () => {
            aFetchCurrencies()
                .then(currencies => {
                    dispatch({
                        type: FETCH_CURRENCIES,
                        payload: currencies
                    })
                });
        },
    };
    
    const currencySelectors = {
        getCurrencies: () => currencyState.currencies || [],
    };

    return {currencySelectors, currencyActions};
}

export default useCurrency;