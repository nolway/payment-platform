import {useContext, useState} from 'react';
import RootContext from '../contexts/rootContext';
import {
    fetchMerchants as aFetchMerchants,
    fetchMerchant as aFetchMerchant,
    updateMerchant as aUpdateMerchant,
    deleteMerchant as aDeleteMerchant,
    sendKbisToReceiveId as aSendKbisToReceiveId,
    FETCH_MERCHANTS,
    FETCH_MERCHANT,
    UPDATE_MERCHANT,
    DELETE_MERCHANT,
    SEND_KBIS_TO_RECEIVE_ID,
    SEND_KBIS_TO_RECEIVE_ID_ERROR
} from '../contexts/actions/merchantAction';
import {useHistory} from 'react-router-dom';

const useMerchant = () => {
    const history = useHistory();
    const {
        state: {
            merchant: merchantState
        },
        dispatch,   
    } = useContext(RootContext);
    
    const merchantActions = {
        fetchMerchants: (token) => {
            aFetchMerchants(token)
                .then(merchants => {
                    dispatch({
                        type: FETCH_MERCHANTS,
                        payload: merchants
                    })
                });
        },
        fetchMerchant: (token, merchantId) => {
            aFetchMerchant(token, merchantId)
                .then(merchant => {
                    dispatch({
                        type: FETCH_MERCHANT,
                        payload: merchant
                    });
                });
        },
        updateMerchant: (token, merchantId, data) => {
            aUpdateMerchant(token, merchantId, data)
                .then(merchant => {
                    dispatch({
                        type: UPDATE_MERCHANT,
                        payload: {
                            merchant,
                            successUpdateMsg: 'Les données ont été mise à jour.'
                        }
                    });
                });
        },
        deleteMerchant: (token, merchantId) => {
            aDeleteMerchant(token, merchantId)
                .then(merchantId => {
                    console.log(merchantId)
                    dispatch({
                        type: DELETE_MERCHANT,
                        payload: merchantId
                    })
                    history.push('/admin/merchants');
                })
        },
        sendKbisToReceiveId: (data) => {
            aSendKbisToReceiveId(data)
                .then(async (data) => {
                    if (data.status === 201) {
                        const dataJson = await data.json();
                        dispatch({
                            type: SEND_KBIS_TO_RECEIVE_ID,
                            payload: {
                                kbisId: dataJson.id
                            }
                        });
                    }
                    else if (data.status === 400) {
                        const dataJson = await data.json();
                        console.log(dataJson.message)
                        dispatch({
                            type: SEND_KBIS_TO_RECEIVE_ID_ERROR,
                            payload: {
                                kbisUploadError: dataJson.message
                            }
                        });
                    }
                });
        }
    };
    
    const merchantSelectors = {
        getMerchants: () => merchantState.merchants,
        getMerchant: () => merchantState.merchant,
        getSuccessUpdateMsg: () => merchantState.successUpdateMsg || '',
        getKbisId: () => merchantState.kbisId || '',
        getKbisUploadError: () => merchantState.kbisUploadError || '',
        filterMerchants: (socialReason) => merchantState.merchants.filter(merchant => 
            merchant.social_reason.toLowerCase().includes(socialReason.toLowerCase())    
        ),
        // filterMerchantTransactions: (price) => merchantState.merchant.filter(transaction => 
        //     transaction.price.toString().includes(price)
        // ),
    };

    return {merchantSelectors, merchantActions};
}

export default useMerchant;