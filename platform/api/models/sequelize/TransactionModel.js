'use strict';

const transactionDenormalisation = require('./denormalization/transaction');
const merchantDenormalisation = require('./denormalization/merchant');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Transaction extends Model {
        static associate(models) {
            Transaction.belongsTo(models.Merchant);
        }
    }

    Transaction.init(
        {
            status: {
                type: DataTypes.STRING(25),
                defaultValue: 'created',
                allowNull: false
            },
            price: {
                type: DataTypes.FLOAT,
                allowNull: false
            },
            currency: {
                type: DataTypes.STRING(3),
                allowNull: false,
                validate: {
                    isUppercase: true
                }
            },
            token: {
                type: DataTypes.STRING(64),
                unique: true,
                allowNull: false
            }
        },
        {
            sequelize,
            modelName: 'Transaction',
            timestamps: true,
            paranoid: true
        }
    );

    Transaction.generateToken = () => {
        return Date.now() + '_' + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    };

    Transaction.addHook("afterCreate", async (transaction) => {
        await Transaction.findOne({where: {id: transaction.id}, include: {all: true}})
            .then(async transactionFinded => {
                await transactionDenormalisation.denormalize(transactionFinded, "create");
                await merchantDenormalisation.denormalize(transactionFinded.Merchant, "create");
            })
            .catch(err => {
                console.log(err);
            });
    });
    
    Transaction.addHook("afterUpdate", async (transaction) => {
        await Transaction.findOne({where: {id: transaction.id}, include: {all: true}})
            .then(async transactionFinded => {
                await transactionDenormalisation.denormalize(transactionFinded, "update");
                await merchantDenormalisation.denormalize(transactionFinded.Merchant);
            })
            .catch(err => {
                console.log(err);
            });
    });

    Transaction.addHook("afterDestroy", async (transaction) => {
        await Transaction.findOne({where: {id: transaction.id}, include: {all: true}})
            .then(async transactionFinded => {
                await transactionDenormalisation.denormalize(transactionFinded, "delete");
                await merchantDenormalisation.denormalize(transactionFinded.Merchant);
            })
            .catch(err => {
                console.log(err);
            });
    });

    return Transaction;
}
