const MerchantSchema = require("../../mongoose/MerchantSchema");

exports.denormalize = async (merchant, operation) => {
  // Delete outdated document
  await MerchantSchema.deleteOne({ id: merchant.id });
  // if not deletion operation
  if (operation !== "delete") {
    // Compute new document

    merchant = await dbpg.Merchant.findByPk(merchant.id, {
      include:[{
        model: dbpg.UserAccount,
        as: 'User'
      },
      {
        model: dbpg.Transaction,
        as: 'Transactions'
      }]
    }).then(merchant => {
      return merchant;
    });
    // Save document
    const doc = new MerchantSchema(merchant.toJSON());
    await doc.save().catch(error => {
      console.log(error);
    });
  }
};
