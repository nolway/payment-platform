const TransactionSchema = require("../../mongoose/TransactionSchema");

exports.denormalize = async (transaction, operation) => {
  // Delete outdated document
  await TransactionSchema.deleteOne({ id: transaction.id });
  // if not deletion operation
  if (operation !== "delete") {
    // Compute new document

    transaction = await dbpg.Transaction.findByPk(transaction.id, {
      include:[dbpg.Merchant]
    }).then(transaction => {
      return transaction;
    });
    // Save document
    const doc = new TransactionSchema(transaction.toJSON());
    await doc.save().catch(error => {
      console.log(error);
    });
  }
};
