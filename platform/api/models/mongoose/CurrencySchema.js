const mongoose = require("mongoose");
const connect = require("../../lib/mongo");

const CurrencySchema = new mongoose.Schema({
    iso: String,
    value: Number
});

const Currency = connect.model("Currency", CurrencySchema);

module.exports = Currency;