const express = require('express');
const cors = require("cors");
const fileUpload = require('express-fileupload');
const https = require('https');
const fs = require('fs');
const verifyJwt = require('./middlewares/verifyJwt');
const scrapper = require('./scrapping');
const app = express();
const port = 3000;


/* db connections */
require('./lib/mongo');
global.dbpg = require('./models/sequelize/');

/* middlewares */ 
app.use(express.json()); // bodyParser
app.use(cors());
app.use(fileUpload());

/* import routes */
require('./routes/users')(app);
require('./routes/transactions')(app);
require('./routes/merchants')(app);
require('./routes/currency')(app);
require('./routes/kpi')(app);

/* server */
// const server = https.createServer({
//     key: fs.readFileSync('selfsigned.key'),
//     cert: fs.readFileSync('selfsigned.crt')
//   },app).listen(port, () => {
//     console.log(`Started at http://localhost: ${port}`);
// });

app.listen(port, () => {
    console.log(`Started at http://localhost:${port}`);
    scrapper.updateCurrencies();
    setInterval(() => scrapper.updateCurrencies(), 86400000);
});