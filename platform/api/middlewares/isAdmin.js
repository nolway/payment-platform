const verifyJwt = require('./verifyJwt');

const isAdmin = (req, res, next) => {
    verifyJwt(req, res, next);

    if (req.user.role === 1) {
        next();
    } else {
        res.status(403).json({
            message: "Unauthorise access"
        });
    }
};

module.exports = isAdmin;
