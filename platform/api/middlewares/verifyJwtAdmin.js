const verifyToken = require('../lib/jwt').verifyToken;

const verifyJwtAdmin = (req, res, next) => {

    const authHeader = req.get('Authorization');

    if (!authHeader || !authHeader.startsWith('Bearer ')) return res.sendStatus(401);
    
    const token = authHeader.replace('Bearer ', '');
    verifyToken(token)
        .then(payload => {
            req.user = payload;
            if (req.user.role !== 1) {
                return res.status(403).json({
                    message: "Unauthorise access"
                });
            }
            next();
        })
        .catch(err => res.status(401).json({token: [err]}));
};

module.exports = verifyJwtAdmin;
