const CurrencySchema = require('../models/mongoose/CurrencySchema');

const prettifyErrors = (errors) => {
    return errors.reduce((acc, item) => {
        acc[item.path] = [...(acc[item.path] || []), item.message];
    
        return acc;
    }, {});
};

exports.findAll = (req, res) => {
    CurrencySchema.find()
        .then(data => res.json(data))
        .catch(err => err.sendStatus(500)
    );
}

exports.findOne = (req, res) => {
    CurrencySchema.findOne({ iso: req.params.iso.toUpperCase() })
        .then(merchant => (merchant ? res.json(merchant) : res.sendStatus(404)))
        .catch(() => res.sendStatus(500)
    )
};
