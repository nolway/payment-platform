module.exports = app => {
    const adminController = require('../controllers/AdminController');
    const logged = require('../middlewares/logged');
    const router = require('express').Router();
    const bodyParser = require('body-parser');
    const jsonParser = bodyParser.json();

    router.get("/login", adminController.login);
    router.get("/logout", adminController.logout);
    router.post("/login-check", adminController.loginCheck);

    router.get("/", logged, adminController.index);
    router.get("/orders", logged, adminController.getOrders);
    router.post("/orders/refund", logged, jsonParser, adminController.refund);

    router.get("/settings", logged, adminController.getSettings);
    router.post("/settings/update", logged, adminController.getSettingsUpdate);

    app.use('/admin', router);
}
