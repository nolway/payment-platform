'use strict';

const axios = require('axios');

exports.transaction = async (req, res) => {
    if (!req.body.price || !req.body.currency || !req.body.user) {
        return res.status(400).end();
    }
    let price = req.body.price;
    let currency = req.body.currency;
    let user = req.body.user;

    if (!user.lastname || !user.firstname || !user.email) {
        return res.status(400).end();
    }

    let client_secret = await db.Setting.findOne({
        where: {
            key: 'client_secret'
        }
    })
    .then(setting => setting)
    .catch((err) => {
        console.log(err);
        return null;
    });

    if (!client_secret) {
        return res.status(500).end();
    }

    let data = {
        price,
        currency,
        client_secret: client_secret.value
    };

    let request = await axios.post('http://platform:3000/api/transactions', data)
        .then(async response => {
            if (response.status !== 201) {
                return res.status(400).end();
            }

            await db.Order.create({
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                price: price,
                currency: currency,
                transaction: response.data.token
            });

            return res.status(201).json({
                url: response.data.url
            }).end();
        })
        .catch(error => {
            console.log(error);

            if (error.response.data && error.response.data.message) {
                return res.status(500).json(error.response.data).end();
            } else {
                return res.status(504).end();
            }
        });
    return request;
}

exports.confirmation = async (req, res) => {
    if (!req.query.t || !req.query.ct) {
        return res.redirect('/');
    }

    let client_token = await db.Setting.findOne({
        where: {
            key: 'client_token'
        }
    })
    .then(setting => setting)
    .catch((err) => {
        console.log(err);
        return null;
    });

    if (!client_token) {
        return res.redirect('/');
    }

    if (client_token.value !== req.query.ct) {
        return res.redirect('/');
    }

    let order = await db.Order.findOne({
        where: {
            transaction: req.query.t
        }
    })
    .then(order => order)
    .catch((err) => {
        console.log(err);
        return null;
    });

    if (!order) {
        return res.redirect('/');
    }

    if (order.status !== "created") {
        console.log('4');
        console.log(order);
        return res.redirect('/');
    }

    order.status = "confirmed";
    await order.save();

    return res.render('confirmation', {
        order
    });
}

exports.cancel = async (req, res) => {
    if (!req.query.t || !req.query.ct) {
        return res.redirect('/');
    }

    let client_token = await db.Setting.findOne({
        where: {
            key: 'client_token'
        }
    })
    .then(setting => setting)
    .catch((err) => {
        console.log(err);
        return null;
    });

    if (!client_token) {
        return res.redirect('/');
    }

    if (client_token.value !== req.query.ct) {
        return res.redirect('/');
    }

    let order = await db.Order.findOne({
        where: {
            transaction: req.query.t
        }
    })
    .then(order => order)
    .catch((err) => {
        console.log(err);
        return null;
    });

    if (!order) {
        return res.redirect('/');
    }

    if (order.status !== "created") {
        return res.redirect('/');
    }

    order.status = "canceled";
    await order.save();

    res.render('cancel', {
        order
    });
}