'use strict';

const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Order extends Model {}

    Order.init(
        {
            firstname: {
                type: DataTypes.STRING(100),
                allowNull: false
            },
            lastname: {
                type: DataTypes.STRING(100),
                allowNull: false
            },
            email: {
                type: DataTypes.STRING(255),
                allowNull: false
            },
            status: {
                type: DataTypes.STRING(25),
                defaultValue: 'created',
                allowNull: false
            },
            price: {
                type: DataTypes.FLOAT,
                allowNull: false
            },
            currency: {
                type: DataTypes.STRING(3),
                allowNull: false,
                validate: {
                    isUppercase: true
                }
            },
            transaction: {
                type: DataTypes.STRING,
                allowNull: false,
            }
        },
        {
            sequelize,
            modelName: 'Order',
            timestamps: true,
            paranoid: true
        }
    );

    return Order;
};
