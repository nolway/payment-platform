'use strict';

const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Setting extends Model {}

    Setting.init(
        {
            key: {
                type: DataTypes.STRING(60),
                allowNull: false
            },
            value: {
                type: DataTypes.STRING(255),
                allowNull: false
            }
        },
        {
            sequelize,
            modelName: 'Setting',
            timestamps: true,
            paranoid: true
        }
    );

    return Setting;
};